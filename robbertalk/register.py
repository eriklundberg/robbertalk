# -*- coding: utf- 8 -*-

"""
robbertalk.register
~~~~~~~~~~~~~~~~~~~

This submodule provides the functionality for registering the codec.
"""

__author__ = 'erik'

import codecs
from .codec import RobberTalkCodec, RobberTalkStreamReader, \
    RobberTalkStreamWriter


def find_robbertalk(encoding):
    if encoding == 'robbertalk':
        return codecs.CodecInfo(
            name='robbertalk',
            encode=RobberTalkCodec().encode,
            decode=RobberTalkCodec().decode,
            streamreader=RobberTalkStreamReader,
            streamwriter=RobberTalkStreamWriter
        )

    return None


def register_codec():
    codecs.register(find_robbertalk)
