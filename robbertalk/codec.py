# -*- coding: utf- 8 -*-

"""
robbertalk.codec
~~~~~~~~~~~~~~~~

This submodule implements the codec as a subclass to the codecs.Codec class.

"""

import re
import codecs

__author__ = 'erik'


class RobberTalkCodec(codecs.Codec):
    consonants = r'[bcdfghjklmnpqrstvwxz]'

    def encode(self, input, errors='strict'):
        return re.sub(r'(' + RobberTalkCodec.consonants + ')', r'\1o\1',
                      input, flags=re.I), len(input)

    def decode(self, input, errors='strict'):
        match = r'(' + RobberTalkCodec.consonants + r')o\1'
        return re.sub(match, r'\1', input), len(input)


class RobberTalkStreamReader(RobberTalkCodec, codecs.StreamReader):
    pass


class RobberTalkStreamWriter(RobberTalkCodec, codecs.StreamWriter):
    pass
