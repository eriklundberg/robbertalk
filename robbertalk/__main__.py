# -*- coding: utf-8 -*-

"""
robbertalk.__main__
~~~~~~~~~~~~~~~~~~~

This submodule is invoked when the module is started from the command line.
It registers the codec, configures stdout to use the codec for output and then
echos stdin to stdout through the codec.

"""

import codecs
import sys
from . import register


def main():
    register.register_codec()
    sys.stdout = codecs.getwriter('robbertalk')(sys.stdout)

    for line in iter(sys.stdin):
        print(line.strip())

if __name__ == '__main__':
    main()
