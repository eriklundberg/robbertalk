# -*- coding: utf-8 -*-

"""
RobberTalk codec
~~~~~~~~~~~~~~~~

This module provides a codec for encoding/decoding the robber language.
"""

from .register import register_codec

__author__ = 'erik'
__all__ = ['codec']

register_codec()
