RobberTalk codec
================

This very useful module provides a codec for coding/decoding the robber
language.

Usage
-----

.. code-block:: python

>>> import codecs
>>> import robbertalk
>>> codecs.encode('don\'t panic', 'robbertalk')
"dodonon'tot popanonicoc"
>>> codecs.decode('momosostotloly hoharormomlolesossos', 'robbertalk')
'mostly harmless'
